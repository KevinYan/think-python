"""
Think Python 2e 练习10-1:
    接收一个由内嵌的整数列表组成的列表作为形参，将内嵌列表中的值全部加起来
"""
def nested_sum(t):
    total = 0
    for item in t:
        if isinstance(item, list):
            total += nested_sum(item)
        else:
            total += item
    return total

if __name__ == '__main__':
    a = [[1, [2, 3]], 4, [5, 6]]
    print(nested_sum(a))
    