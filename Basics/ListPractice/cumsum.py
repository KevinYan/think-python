git """
Think Python 2e 练习10-2：
    编写一个函数，接收一个数字的列表， 返回累计和；也就是说返回一个新的列表，
    其中第i 个元素是原先列表的前i个元素的和
    
练习 10-3:
    编写一个函数，接收一个列表，并返回一个新列表，包含除了第一个和最后一个元素的所有元素
    
练习 10-4:
    接收一个列表，修改它，删除它的第一个和最后一个元素并返回None
  
练习 10-5：
    write a function called is_sorted that takes a list as a parameter and returns
    True if the list is sorted in ascending order and False otherwise.
"""
def cumsum(t):
    res = []
    i = 0
    while i < len(t):
        total = 0
        j = 0
        while j <= i:
            total += t[j]
            j = j + 1
        res.append(total)
        i = i + 1
    return res

def middle(t):
    start = 1
    end   = len(t) - 1
    new_list = t[start:end]#切片操作会新建一个列表
    return new_list

def chop(t):
    t.pop(0)
    t.pop(len(t) - 1)
    #也可以用del
    #del t[0]
    #del t[len(t) - 1]
    
def is_sorted(t):
    """Check whether a list is sorted
    
    t: list
    
    return: None
    """
    return t == sorted(t)
        
        
if __name__ == '__main__':
    a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    new_list = cumsum(a)
    print(new_list)
    print(middle(a), a)
    print(chop(a), a)
    print(is_sorted(a))
        