"""
Think Python 2nd Edition
练习10-12:
    两个单词，如果从每个单词中交错取出一个字母可以组成一个新的单词，我们称它们为“互锁”(interlocking). 例如，
    "shoe"和"cold"可以互锁组成单词"schooled"。
    
    1. 编写一个程序找到所有互锁的词。提示: 不要穷举所有的词对！
    2. 能不能找到“三互锁”的单词？也就是，从第一、第二或者第三个字母开始，每第三个字母喝起来可以形成一个单词
    
    提示1：可以把单词表里的单词按照互锁的规则拆成两个， 然后再去单词表里查找是否包含拆分成的两个单词
"""


from inlist import make_word_list, in_bisect

def interlock(word_list, word):
    """Checks whether a word contains two interleaved words.
    
    word_list: list of strings
    word: string
    
    检查单词是否包含隔行交错存储的单词
    """
    evens = word[::2]#由奇数索引组成的单词
    odds  = word[1::2]#由偶数索引组成的单词
    
    return in_bisect(word_list, evens) and in_bisect(word_list, odds)


def interlock_general(word_list, word, n=3):
    """Checks whether a word contains n interleaved words.
    
    word_list: list of strings
    word: string
    n: number of interleaved words
    """
    for i in range(n):
        inter = word[i::n]
        if not in_bisect(word_list, inter):
            return False
    return True


if __name__ == '__main__':
    word_list = make_word_list()
    
    for word in word_list:
        if interlock(word_list, word):
            print(word, word[::2], word[1::2])
            
    for word in word_list:
        if interlock_general(word_list, word, 3):
            print(word, word[0::3], word[1::3], word[2::3])
    
