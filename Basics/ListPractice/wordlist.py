#!/usr/bin/python
# -*- coding=UTF-8 -*-

"""
Think Python 2nd Edition
Paractice 10-9:
编写一个函数，读取文件words.txt, 并构建一个列表，每个元素是一个单词。给这个函数编写两个版本，
其中一个使用append方法，另一个使用 t = t + [x] 的用法。哪一个运行时间更长? 为什么?
"""
from __future__ import print_function, division
import os, time

words_file = os.path.realpath('../StringPractice/words.txt')



def append_build_list():
    start = time.time()
    t = []
    for line in open(words_file):
        word = line.strip()
        t.append(word)
    end = time.time()
    print(end - start)

def slice_build_list():
    start = time.time()
    t = []
    for line in open(words_file):
        word = line.strip()
        t += [word]
    end = time.time()
    print(end - start)

if __name__ == '__main__':
    append_build_list()
    slice_build_list()

    # 结论， t = t + [word] 这种使用切片构建列表的方法更高效