"""
Think Python 2e
练习10-10:
    通过二分查找法检查单词是否在单词列表中
    二分查找：先查看中间的单词，看中间单词是否在中间单词之前，如果时再用二分查找法查找前半段单词列表，
    否则用二分查找法查找后半部分的单词。
    如果查找到了目标单词返回它在单词列表里的索引， 否则返回False
    
    也可以图省事查看bisect模块来完成查找
"""
from anagrams import makeListFromWordV1
import bisect

def in_bisect(word_list, word):
    """Check whether a word is in a list using bisection search
    
    Precondition: the words in the list are sorted
    word_list: list of strings
    word: string
    """
    if len(word_list) == 0:
        return False
    
    i = len(word_list) // 2
    
    if word_list[i] == word:
        return True
    elif word_list[i] > word:
        #search the first half
        return in_bisect(word_list[:i], word)
    else:
        #seach the second half
        return in_bisect(word_list[i+1:], word)
    
def in_bisect_cheat(word_list, word):
    """Check whether a word is in a list using bisection search
    
    Precondition: the words in the list are sorted
    word_list: list of strings
    word: string
    """
    i = bisect.bisect_left(word_list, word)
    if i == len(word_list):
        return False
    
    return word_list[i] == word
        
    
if __name__ == '__main__':
    print(in_bisect(makeListFromWordV1(), 'absolute'))
    