#!/usr/bin/python
# -*- coding=UTF-8 -*-

"""
Think Python 2dn Edition
Paractice 10-8

如果你的班级中有23个学生，那么其中有两个人生日相同的概率有多大？你可以通过
随机生成23个生日的样本并检查是否有相同的匹配来估计这个概率。
提示：可以使用random模块中的randint函数来生成随机生日。
"""

from __future__ import print_function, division
import random

def has_duplicates(t):
    """Returns True if any elements appears more than once in a sequence.
    
    t: list
    returns: bool
    """
    # make a copy of t to avoid modifying the parameter
    s = t[:]
    s.sort()

    #check for adjacent elements that are equal
    for i in range(len(s) - 1):
        if s[i] == s[i+ 1]:
            return True
    return False

def random_bdays(n):
    """Returns a list of integers between 1 and 365, with length n

    n: int
    returns: list of int
    """
    t = []
    for i in range(n):
        bday = random.randint(1, 365)
        t.append(bday)
    return t

def count_matches(num_students, num_simulations):
    """Generate a sample of birthdays and counts duplicates

    num_students: how many students in the group
    num_simulations: how many groups to simulate

    returns: int
    """
    count = 0
    for i in range(num_simulations):
        t = random_bdays(num_students)
        if has_duplicates(t):
            count += 1
    return count

def main():
    """Runs the birthday simulation and prints the number of matches."""
    num_students = 23
    num_simulations = 1000
    count = count_matches(num_students, num_simulations)

    print('After %d simulations' % num_simulations)
    print('each with %d students' % num_students)
    print('there were %d simulations with at least one match' % count)     

if __name__ == '__main__':
    main()   