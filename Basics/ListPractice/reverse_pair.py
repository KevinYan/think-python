"""
Think Python 2nd Edition
练习 10-11
查找出单词表中互为反向对的单词
"""

from bisection_search import in_bisect
from anagrams import makeListFromWordV1

def reverse_pair():
    word_list = makeListFromWordV1()
    result = []
    for line in open('/home/coding/workspace/Basics/StringPractice/words.txt'):
        word = line.strip()
        word1 = word[::-1]
        if in_bisect(word_list, word1):
            result.append(word)
            
    return result


if __name__ == '__main__':
    print(reverse_pair())
    