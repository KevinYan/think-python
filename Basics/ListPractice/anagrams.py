"""
Think Python 2e
练习10-6 写一个函数，接受两个字符串参数， 如果两个字符串是由颠倒字母顺序而构成则返回True，否则返回False

练习10-7 写一个函数， 接受一个列表参数， 如果列表中有元素出现一次以上返回True, 否则返回False

练习10-9 写一个函数读取文件words.txt, 以每个单词为一列表元素来构建列表。 此函数需编写两个版本
一个使用列表方法append, 另一个使用"+"操作符。 哪个版本的函数耗费的时间更长， 为什么。
"""
import time

def is_anagram(word1, word2):
    word3 = word2[::-1]#通过切片操作创建一个与word2字母颠倒的字符串
    return word1 == word3

def has_duplicates(t):
    """
    t: list
    """
    count_list = []
    for item in t:
        if item in count_list:
            return True
        else:
            count_list.append(item)
    return False

def makeListFromWordV1():
    start_time = time.time()
    t = []
    for line in open('/home/coding/workspace/Basics/StringPractice/words.txt'):
        t.append(line.strip())#line.strip()去掉行尾的换行符
    end_time = time.time()
    #print(end_time - start_time)  
    return  t

def makeListFromWordV2():
    start_time = time.time()
    t = []
    for line in open('/home/coding/workspace/Basics/StringPractice/words.txt'):
        t = t + [line.strip()]
    end_time = time.time()
    # print(end_time - start_time)
    return t
        

if __name__ == '__main__':
    print("is anagram:", is_anagram('abc', 'cba'))
    print("has duplicates:", has_duplicates('aluminum'))
    print(makeListFromWordV1())
    print(makeListFromWordV2())
    
    