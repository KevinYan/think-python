from __future__ import print_function, division
import math

"""
计算平方根的牛顿方法: 假设你想要知道a的平方根，如果以任意一个估值x开始，可以使用如下的方程获得一个更好的估计值y
y =(x + a / x）/ 2
"""

def mysqrt(a):
    """使用牛顿方法获取a的平方根"""
    x = 3
    epsilon = 0.0000001
    while True:
        y = (x + a / x) / 2
        if y == x:
            break
        if abs(y - x) < epsilon:
            break    
        x = y
    return y

def test_square_root():
    print('a', 'mysqrt(a)', 'math.sqrt(a)', 'diff', sep = ' ' * 16)
    print('-', '-' * 9, '-' * 12, '-' * 4, sep = ' ' * 16)
    print(1.0, mysqrt(1.0), math.sqrt(1.0), math.sqrt(1.0) - mysqrt(1.0), sep = ' ' * 16)
    print(2.0, mysqrt(2.0), math.sqrt(2.0), math.sqrt(2.0) - mysqrt(2.0), sep = ' ' * 16)
    print(3.0, mysqrt(3.0), math.sqrt(3.0), math.sqrt(3.0) - mysqrt(3.0), sep = ' ' * 16)
    print(4.0, mysqrt(4.0), math.sqrt(4.0), math.sqrt(4.0) - mysqrt(4.0), sep = ' ' * 16)

if __name__ == '__main__':
    test_square_root()


    



