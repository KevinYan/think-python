"""
接收一个字符串按照频率的降序打印字母。 从不同的语言中查找文本样例并查看不同语言中的单词频率的变化
"""

def most_frequent(s):
    """Sorts the letters in s in reverse order of frequency.
    
    s: string
    
    Return: list of letters
    """
    hist = make_histogram(s)
    t = []
    for x, freq in hist.items():
        t.append((freq, x))
    t.sort(reverse=True)
    
    res = []
    for freq, x in t:
        res.append(x)
    
    return res


def make_histogram(s):
    """Make a map from letters to number of times they appear in s.
    
    s: string
    
    Return: map from letter to frequency
    """
    hist = {}
    for x in s:
        hist[x] = hist.get(x, 0) + 1
        
    return hist
    
    

def read_file(filename):
    """Returns the contents of a file as a string"""
    return open(filename).read()

if __name__ == '__main__':
    string = read_file('/home/coding/workspace/Basics/TuplePractice/emma.txt')
    letter_seq = most_frequent(string)
    for x in letter_seq:
        print(x)