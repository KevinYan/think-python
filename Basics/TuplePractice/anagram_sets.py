"""
编写一个程序从文件中读取一个单词列表，并打印出所有是回文的单词集合
下面是输出样例：
['deltas', 'desalt', 'lasted']
['resmelts', 'smelters', 'termless']
"""

def signature(s):
    """Returns the signature of this string
    
    Signature is a string that contains all of the letters in order.
    
    s: string
    """
    t = list(s)
    t.sort()
    t = ''.join(t)
    return t


def all_anagrams(filename):
    """Finds all anagrams in a list of words.
    
    filename: string filename of the word list
    
    Returns: a map from each word to a list of its anagrams.
    """
    d = {}
    for line in open(filename):
        word = line.strip().lower()
        t = signature(word)
        d.setdefault(t, []).append(word)
    return d


def print_anagram_sets(d):
    """Prints the anagram sets in d.
    
    d: map from words to list of their anagrams
    """
    for v in d.values():
        if len(v) > 1:
            print(len(v), v)
            
            
def print_anagram_sets_in_order(d):
    """Prints the anagram sets in d in desending order of size.
    
    d: map from words to list of their anagrams
    """
    t = []
    for v in d.values():
        if len(v) > 1:
            t.append((len(v), v))
            
    #sort in ascending order of length
    t.sort()
    #print the sorted list
    for x in t:
        print(x)
        

def filter_length(d, n):
    """Select only the words in d that have n letters.
    
    d: map from word to list of their anagrams
    
    returns: new map from word to list of anagrams
    """
    
    res = {}
    for word, anagrams in d.items():
        if len(word) == n:
            res[word] = anagrams  
    return res






if __name__ == '__main__':
    d = all_anagrams('/home/coding/workspace/Basics/TuplePractice/words.txt')
    #print_anagram_sets_in_order(d)
    
    eight_letters = filter_length(d, 8)
    print_anagram_sets_in_order(eight_letters)
    