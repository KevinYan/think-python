#!/usr/bin/python
# -*- coding=UTF-8 -*-
"""
Think Python 2nd Edtion Practice9-7
找出单词列表中包含连续的三组成对字母的单词
"""
from __future__ import print_function, division
import os 

def is_tripple_double(word):
    """Tests if  a word contains three consecutive double letters.

    word: string

    returns: bool
    """
    i = 0
    count = 0
    while i < len(word) - 1:
        if word[i] == word[i+1]:
            count = count + 1
            if count == 3:
                return True
            i = i + 2
        else:
            # reset count to zero
            count = 0
            i = i + 1
        return False

def find_tripple_double():
    """"Reads a word list and prints words with tripple double letters"""
    dir_path = os.path.dirname(os.path.realpath(__file__))
    fin = open(dir_path + '/words.txt')
    for line in fin:
        word = line.strip()
        if is_tripple_double(word):
            print(word)

print("Here are all the words in the list that have")
print("three consecutive double letters.")
find_tripple_double()