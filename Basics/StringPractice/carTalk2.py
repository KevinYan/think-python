#!/usr/bin/python
# -*- coding=UTF-8 -*-

from __future__ import print_function, division

def has_palindrome(i, start, length):
    """Checks if the string presentation of i has a palindrome.

    i: integer
    start: where in the string to start
    length: length of the palindrome to check for
    """
    s = str(i)[start:start + length]
    return s[::-1] == s

def check(i):
    """Checks if the integer has the desired properties.
    i: int
    """
    return (has_palindrome(i, 2, 4) and has_palindrome(i + 1, 1, 5) and (i + 2, 1, 4) and has_palindrome(i + 3, 0, 6))

def check_all():
    """Enumerate the six-digit numbers and print any winners.
    """
    i = 10000
    while i <= 999996:
        if check(i):
            print(i)
        i = i + 1

print('The following are the possible odometer readings:')
check_all()
print()