#!/usr/bin/python
# -*- coding=UTF-8 -*-

"""
下面是一个谜题：
最近我去母亲家时，发现自己的年龄的两位数正好是母亲的年龄的两位数的倒序。例如他是73岁我是37岁
回家后我发现我们的年龄互为倒序已经发生了6次。如果顺利的话总共可能发生8次，所以问题是我现在年龄多大？
"""

from __future__ import print_function, division

def str_fill(i, n):
    """Returns i as a string with at least n digits.

    i: int
    n: int length

    returns: string
    """
    return str(i).zfill(n)

def are_reversed(i, j):
    """Checks if i and j are the reversed of each other

    i: int
    j: int

    returns: bool
    """
    return str_fill(1, 2) == str_fill(j, 2)[::-1]

def num_instances(diff, flag=False):
    """Counts the number of the palindromic ages.

    Returns the number of times the mother and daughter have
    palindromic ages in their lives. given the difference in age.

    diff: int difference in ages
    flag: bool, if True, prints the details
    """
    daughter = 0
    count = 0
    while True:
        mother = daughter + diff
        # assuming that mother and daughter don't have the same birthday.
        # they have two chances per year to have palindromic ages.
        if are_reversed(daughter, mother) or are_reversed(daughter, mother + 1):
            count = count + 1
            if flag:
                print(daughter, mother)
        if mother > 120:
            break
        daughter = daughter + 1
    return count

def check_diffs():
    """Finds age differences that satisfay the problem.

    Enumerate the possible differences in age between mother 
    and daughter, and for each difference, counts the number of times
    over their lives they will ages that are the reversed of each other.
    """
    diff = 10
    while diff < 70:
        n = num_instances(diff  )
        if n > 0:
            print(diff, n)
        diff = diff + 1

print('diff #instances')
check_diffs()

print()
print('daughter mother')
num_instances(18, True)