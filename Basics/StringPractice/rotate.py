#!/usr/bin/python
# -*- coding: UTF-8 -*-

"""
Think Python 2nd Edition

Caesar Cypher(凯撒密码)：
    凯撒密码是一个比较弱的加密形式，它涉及将单词中的每个字母“轮转”固定数量的位置。
轮转一个字母意思是在字母表中移动它，如果需要，再从头开始。所以'A'轮转3个位置是'D',
而'Z'轮转一个位置是'A'

编写一个函数rotate_word，接受一个字符串以及一个整数作为参数，返回一个新字符串，其中
的字母按照给定的数值“轮转”位置

"""

def rotate_letter(letter, n):
    """Rotates a letter by n places. Does not change other chars.
    
    letter: single letter string
    n: int
    
    Return: single letter string
    """
    if letter.isupper():
        start = ord('A')
    elif letter.islower():
        start = ord('a')
    else:
        return letter
    
    c = ord(letter) - start
    i = (c + n) % 26 + start
    
    return chr(i)

def rotate_word(word, n):
    """Rotates a word by n places.
    
    word: string
    n: integer
    
    Return: string
    """
    res = ''
    for letter in word:
        res += rotate_letter(letter, n)
    return res


if __name__ == '__main__':
    print(rotate_word('cheer', 7))
    print(rotate_word('melon', -10))
    print(rotate_word('sleep', 9))