"""
Think Python 2e 练习9-3:
编写avoids函数，接收一个单词，以及包含禁止字母的字符串， 当单词不包含任何禁止字母时，返回True
"""

def avoids(word, forbidden):
    for letter in word:
        if letter in forbidden:
            return False
    return True

def use_only(word, available):
    for letter in word:
        if letter not in available:
            return False
    return True

def use_all(word, required):
    return use_only(required, word)
    

if __name__ == '__main__':
    print(avoids('test', 'es'))
    print(avoids('test', 'abc'))