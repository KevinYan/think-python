#!/usr/bin/python
# -*- coding=UTF-8 -*-

"""编写一个名为is_abecedarian的函数，如哦单词中的字母是按照字母表的顺序排列的(两个字母重复也可以)，
则返回True。
"""
def is_abecedarian(word):
    previous = word[0]
    for letter in word:
        if letter < previous:
            return False
        previous = letter
    return True 