"""
Think Python 2e 练习9-2:
    打印出单词表中不包含"e"的单词， 并计算这种单词在整个单词表中的百分比
"""

def has_no_e(word):
    """
    如果给定的单词中不包含字母"e"则返回True， 否则返回False
    """
    if 'e' in word:
        return False
    else:
        return True
    

count = 0
count_without_e = 0
for line in open('words.txt'):
    line = line.strip()
    if has_no_e(line):
        count_without_e = count_without_e + 1
        print(line)
    count = count + 1
    

percent = round(count_without_e / count, 2)    
print('Percent of word without "e": ' + str(percent))
    