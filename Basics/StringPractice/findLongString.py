"""
Think Python 2e 练习9-1:
    打印出长度超过20个字符（不包括空白字符）的单词
"""
for line in open('words.txt'):
    line = line.strip()
    if len(line) > 20:
        print(line)