"""
检查费马大定律是否成立
费马大定律：对于任何大于2的n, 不存在任何正整数a、b、c能满足：a**n + b**n = c**n
"""

def check_fermat(a, b, c, n):
    n = int(n)
    if n <= 2:
        print("请确保n大于2的正整数")
        
    if a**n + b**n == c**n:
        print("天哪，费马弄错了")
    else:
        print("不，那样不行, 费马是正确的")
        
def run_check_fermat():
    a = int(input("请输入a的值(正整数)\n"))
    b = int(input("请输入b的值(正整数)\n"))
    c = int(input("请输入c的值(正整数)\n"))
    n = int(input("请输入n的值(正整数)\n"))
    
    check_fermat(a, b, c, n)
   


if __name__ == '__main__':
    run_check_fermat()
    