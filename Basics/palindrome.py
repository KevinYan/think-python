from __future__ import print_function, division

def first(word):
    """Return the first character of a string"""
    return word[0]

def last(word):
    """Return the last character of a string"""
    return word[-1]

def middle(word):
    """Return all but the first and last characters of a string"""
    return word[1:-1]

def is_palindrome(word):
    """Return True if word is a palindrome"""
    if len(word) <= 1:
        return True
    if first(word) != last(word):
        return False
    return is_palindrome(middle(word))

if __name__ == '__main__':
    print(is_palindrome('allen'))
    print(is_palindrome('bob'))
    print(is_palindrome('otto'))
    print(is_palindrome('redivider'))