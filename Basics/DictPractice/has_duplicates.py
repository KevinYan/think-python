"""
检查列表中是否有值相同的重复元素
"""

def has_duplicates(t):
    """
    t: sequence
    """
    d = {}
    for x in t:
        if x in d:
            return True
        else:
            d[x] = True
    return False


def has_duplicates2(t):
    """
    t: sequence
    """
    return len(set(t)) < len(t)


if __name__ == '__main__':
    t = [1, 2, 3]
    print(has_duplicates(t))
    t.append(1)
    print(has_duplicates(t))
    