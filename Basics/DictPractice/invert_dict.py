"""
将一个字典的键值进行互换，
互换后值作为键会映射一个包含先前键的列表

use dict method setdefault:
This method returns the key value available in the dictionary and if given key is not available then it will return provided default value.
"""

def invert_dict(d):
    """Inverts a dictionary, returning a map from val to a list of keys.
    
    If the mapping key->val appears in d, then in the new dictionary 
    val maps to a list that includes key
    
    d: dict
    
    Return: dict
    """
    inverse = {}
    for key in d:
        val = d[key]
        inverse.setdefault(val, []).append(key)
    return inverse

if __name__ == '__main__':
    d = dict(a=1, b=2, c=3, d=4, e=5, z=5)
    inverse = invert_dict(d)
    for val in inverse:
        keys = inverse[val]
        print(val, keys)
    
    