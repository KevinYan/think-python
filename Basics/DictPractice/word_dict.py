"""
Think Python 2nd edition
Practice:10-1
"""
import time
import sys
import os
sys.path.append('/home/coding/workspace/Basics/ListPractice')
import inlist
def make_word_dict():
    word_dict = dict()
    word_txt_file = words_file = os.path.realpath('../StringPractice/words.txt')
    for line in open(word_txt_file):
        word = line.strip()
        word_dict[word] = True
        
    return word_dict


if __name__ == '__main__':
    word_dict = make_word_dict()
    find_dict_start = time.time()
    find_result = 'test' in word_dict
    find_dict_end = time.time()
    word_list = inlist.make_word_list()
    find_list_start = time.time()
    inlist.in_bisect(word_list, 'test')
    find_list_end = time.time()
    print('Find dict use:', find_dict_end - find_dict_start)
    print('Find list use:', find_list_end - find_dict_start)
    
        
