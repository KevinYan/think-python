"""
Think python 2e paractice 7-2
"""

import math

def eval_loop():
    while True:
        line = input('Math expression: ')
        if line == 'done':
            break
        result = eval(line)
        print(result)
        
    return result

if __name__ == '__main__':
    eval_loop()