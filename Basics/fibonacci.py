"""
斐波纳契数列以如下被以递归的方法定义：F(0)=0，F(1)=1, F(n)=F(n-1)+F(n-2)（n>=2，n∈N*）

"""

def fibonacci (n):
    if not isinstance(n, int):
        print('Fibonacci number is only defined for integers')
        return None
    elif n < 0:
        print('Fibonacci number is not defined for negative integers')
        return None
    elif n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibonacci(n - 1) + fibonacci(n - 2)
    
    
  
if __name__ == '__main__':
    #从F(0)到F(9)
    for i in range(10):
        result = fibonacci(i)
        print(result, end=' ')
    print('')    
    
