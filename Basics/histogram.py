"""
求的字符串中字符出现次数的直方图
"""

def histogram(s):
    """s: string
    """
    d = dict()
    for c in s:
        if c not in d:
            d[c] = 1
        else:
            d[c] += 1
    return d


def histogram_v2(s):
    """s: string
    """
    d = dict()
    for c in s:
        d[c] = d.get(c, 0) + 1
    return d    
        
def reverse_lookup(d, v):
    """方向查找字典是否包含指定的值
    d: dictionary
    """
    for k in d:
        if d[k] == v:
            print(k, d[k], v)
            return k
    raise LookupError('value does not appear in the dictionary')

if __name__ == '__main__':
    print(histogram('paleontologist'))
    print(histogram_v2('therapy'))
    h = histogram('paleontologist')
    print(reverse_lookup(h, 1))
    print(reverse_lookup(h, 'z'))