"""
用递归实现阶乘函数
阶乘函数说明：0的阶乘是1，而任意其他值n的阶乘是n-1的阶乘乘以n. 即：
           0! = 1
           n! = n * (n - 1)!
"""

def factorial(n):
    if not isinstance(n, int):
        print('Factorial is only defined for integers')
        return None
    elif n < 0:
        print('Factorial is not defined for negative integers')
        return None
    elif n == 0:
        return 1
    else:
        return n * factorial(n - 1)
    
   

if __name__ == '__main__':
    result = factorial(10)
    print(result)