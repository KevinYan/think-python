"""
Think Python 2nd Edition 练习5-1
显示从UNIX纪元到现在过了多少天、小时、分钟、秒
"""
import time
current_time = time.time()
days = int(current_time) // 86400
seconds = int(current_time) % 86400
hours = seconds // 3600 + 8 #东八区
minute_seconds = seconds % 3600
minutes = minute_seconds // 60
real_seconds = minute_seconds % 60
print(hours)
print("天数：" + str(days))
print("小时：" + str(hours))
print("分钟：" + str(minutes))
print("秒： " + str(real_seconds))