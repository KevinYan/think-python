


def check_triangle(a, b, c):
    if a > b + c or b > a + c or c > a + b:
        return 'NO'
    else:
        return 'YES'
    
  
def run_check_triangle():
    a = int(input('请输入三角形第一条边的长度: '))
    b = int(input('请输入三角形第二条边的长度: '))
    c = int(input('请输入三角形第三条边的长度: '))
    
    print(check_triangle(a, b, c))
    

if __name__ == '__main__':
    run_check_triangle()

    
