#!/usr/bin/python
# -*- coding=UTF-8 -*-
"""
Think Python 2nd Edition 
Paractice 14-2
"""
import shelve
import sys
import os

sys.path.append(os.path.realpath('../TuplePractice'))

from anagram_sets import all_anagrams, signature

def store_anagrams(filename, anagram_map):
    """Stores the anagrams from a dictionary in a shelf.

    filename: string file name of shelf
    anagram_map: dictionary that maps string to list of anagrams
    """
    shelf = shelve.open(filename, 'c')
        
    for word, word_list in anagram_map.items():
        shelf[word] = word_list

    shelf.close()

def read_anagrams(filename, word):
    """Looks up a word in shelf and returns a list of its anagrams.

    filename: string file name of shelf
    word: word to look up
    """

    shelf = shelve.open(filename)
    sig = signature(word)
    try:
        return shelf[sig]
    except KeyError:
        return []

def main(script, command='make_db'):
    if command == 'make_db':
        anagram_map = all_anagrams('/home/coding/workspace/Basics/StringPractice/words.txt')
        store_anagrams('anagrams.db', anagram_map)
    else:
        print(read_anagrams('anagrams.db', command))

if __name__ == '__main__':
    main(*sys.argv)