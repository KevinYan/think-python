#!/usr/bin/python
# -*- coding=UTF-8 -*-

"""
Think Python 2nd Edition
Paractice 14-1

写一个名为sed的函数，接收如下参数：一个要查找的字符串，一个替换用字符串，以及两个文件名。
它应该读取第一个文件，并将内容写到第二个文件（如果需要则创建它）。
如果文件中任何一个地方出现了要查找的字符串应该替换掉它。
"""

from __future__ import print_function, division

def sed(pattern, replace, source, dest):
    """
    pattern: string we want to search
    replace: string to replace pattern string
    source: name of file that contains source content
    dest: name of file which will be write replaced content
    """
    fin = open(source, 'r')
    fout = open(dest, 'w')

    for line in fin:
        line = line.replace(pattern, replace)
        fout.write(line)

    fin.close()
    fout.close()

def main():
    pattern = 'pattern'
    replace = 'replace'
    source = 'sed_tester.txt'
    dest = source + '_replaced.txt'
    sed(pattern, replace, source, dest)

if __name__ == '__main__':
    main()